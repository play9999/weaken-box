package life.weaken.box.shulker;

import life.weaken.box.shulker.storage.ShulkerRoom;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public record ShulkerListener(ShulkerManager manager) implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(!event.hasBlock() || event.getClickedBlock() == null) return;
        ShulkerRoom room = manager.getRoom(event.getPlayer());
        if(manager.getShulkerBlock().equals(event.getClickedBlock().getLocation())) {
            event.setCancelled(true);
            if(room != null) return;
            ShulkerRoom freeRoom = manager.getFreeRoom();
            if(freeRoom == null) {
                event.getPlayer().sendMessage(ChatColor.RED + "There is currently no shulker rooms available, please try again later");
                return;
            }
            freeRoom.join(event.getPlayer());
        }
        else if (room != null && room.getButtonloc().equals(event.getClickedBlock().getLocation())) {
            room.free();
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        ShulkerRoom room = manager.getRoom(event.getPlayer());
        if(room == null) return;
        event.setCancelled(true);
        event.getPlayer().sendMessage(ChatColor.RED + "You may not use commands while in a shulker room");
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        ShulkerRoom room = manager.getRoom(event.getPlayer());
        if(room != null) room.free();
    }

}
