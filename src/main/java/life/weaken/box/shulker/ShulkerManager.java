package life.weaken.box.shulker;

import life.weaken.box.shulker.storage.ShulkerRoom;
import life.weaken.box.shulker.storage.ShulkerStorage;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class ShulkerManager {

    private final ShulkerStorage storage;

    public ShulkerManager(Plugin plugin) {
        storage = new ShulkerStorage(plugin);
    }

    public void shutdown() {
        storage.shutdown();
    }

    public ShulkerRoom getFreeRoom() {
        for(ShulkerRoom room : storage.getShulkerRooms().values()) if(room.isAvailable()) return room;
        return null;
    }

    public ShulkerRoom getRoom(Player player) {
        for(ShulkerRoom room: storage.getShulkerRooms().values()) if(!room.isAvailable() && room.getPlayer() == player) return room;
        return null;
    }

    public ShulkerRoom getRoom(String id) {
        return storage.getShulkerRooms().get(id.toUpperCase());
    }

    public void setFreeLocation(Location loc) {
        storage.setFreeLocation(loc);
    }

    public Location getShulkerBlock() {
        return storage.getShulkerBlock();
    }

    public void setShulkerBlock(Location loc) {
        storage.setShulkerBlock(loc);
    }

    public void registerRoom(String id, ShulkerRoom room) {
        storage.getShulkerRooms().put(id.toUpperCase(), room);
    }

    public void unregisterRoom(String id) {
        storage.getShulkerRooms().remove(id.toUpperCase());
    }

    public ShulkerStorage getStorage() {
        return storage;
    }
}
