package life.weaken.box.shulker.storage;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ShulkerStorage {

    private final Map<String, ShulkerRoom> shulkerRooms;
    private final YamlConfiguration storage;
    private final File file;

    private final BukkitTask saveTask;

    private Location freeLocation;
    private Location shulkerBlock;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public ShulkerStorage(Plugin plugin) {

        file = new File(plugin.getDataFolder(), "rooms.yml");
        if(!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        shulkerRooms = new HashMap<>();
        storage = YamlConfiguration.loadConfiguration(file);

        load();

        saveTask = new BukkitRunnable() {
            @Override
            public void run() {
                save();
            }
        }.runTaskTimerAsynchronously(plugin, 6000L, 6000L);
    }

    private void load() {
        freeLocation = storage.getLocation("free-location");
        shulkerBlock = storage.getLocation("block-location");

        ConfigurationSection rooms = storage.getConfigurationSection("rooms");
        if(rooms == null) return;

        for(String room : rooms.getKeys(false)) {
            shulkerRooms.put(room, new ShulkerRoom(this,
                    rooms.getLocation(room + ".teleport"),
                    rooms.getLocation(room + ".button")
            ));
        }

        if(freeLocation == null) freeLocation = new Location(Bukkit.getWorld("world"), 0, 0, 0);
    }

    private void save() {
        storage.set("free-location", freeLocation);
        storage.set("block-location", shulkerBlock);
        for(String room : shulkerRooms.keySet()) {
            ShulkerRoom sroom = shulkerRooms.get(room);
            storage.set("rooms." + room + ".teleport", sroom.getTploc());
            storage.set("rooms." + room + ".button", sroom.getButtonloc());
        }
        try {
            storage.save(file);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void shutdown() {
        saveTask.cancel();
        save();
    }

    public Location getFreeLocation() {
        return freeLocation;
    }

    public void setFreeLocation(Location loc) {
        freeLocation = loc;
    }

    public Location getShulkerBlock() {
        return shulkerBlock;
    }

    public void setShulkerBlock(Location loc) {
        shulkerBlock = loc;
    }

    public Map<String, ShulkerRoom> getShulkerRooms() {
        return shulkerRooms;
    }

}
