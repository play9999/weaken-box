package life.weaken.box.shulker.storage;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class ShulkerRoom{

    private ShulkerStorage storage;

    private boolean available;
    private Location tploc;
    private Location buttonloc;
    private Player player;

    public ShulkerRoom(ShulkerStorage storage, Location tploc, Location buttonloc) {
        this.storage = storage;

        this.tploc = tploc;
        this.buttonloc = buttonloc;

        this.available = true;
    }

    public void setTpLoc(Location loc) {
        this.tploc = loc;
    }

    public void setButtonLoc(Location loc) {
        this.buttonloc = loc;
    }

    public Location getButtonloc() {
        return buttonloc;
    }

    public Location getTploc() {
        return tploc;
    }

    public void free() {
        player.teleport(storage.getFreeLocation());

        available = true;
        player = null;
    }

    public void join(Player player) {
        player.teleport(tploc);

        this.player = player;
        this.available = false;
    }

    public boolean isAvailable() {
        return available;
    }

    public Player getPlayer() {
        return player;
    }

}
