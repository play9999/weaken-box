package life.weaken.box.shulker;

import life.weaken.box.shulker.storage.ShulkerRoom;
import org.bukkit.ChatColor;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public record ShulkerCommand(ShulkerManager manager) implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "This command can only be used by players");
            return true;
        }

        if(args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " help");
            return true;
        }

        if(args[0].equalsIgnoreCase("setfreeloc")) {
            manager.setFreeLocation(player.getLocation());
            sender.sendMessage(ChatColor.GREEN + "Set free loc to your current position");
            return true;
        }
        else if(args[0].equalsIgnoreCase("setblock")) {
            Block block = player.getTargetBlockExact(5, FluidCollisionMode.NEVER);
            if(block == null) {
                sender.sendMessage(ChatColor.RED + "You must be looking at a block to use this command");
                return true;
            }

            manager.setShulkerBlock(block.getLocation());
            sender.sendMessage(ChatColor.GREEN + "Set shulker block to the block you're looking at");
            return true;
        }
        else if(args[0].equalsIgnoreCase("create")) {
            if(args.length < 2) {
                sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " create <id>");
                return true;
            }

            if(manager.getRoom(args[1]) != null) {
                sender.sendMessage(ChatColor.RED + "A room with that id already exists");
                return true;
            }

            manager.registerRoom(args[1], new ShulkerRoom(manager.getStorage(),
                    new Location(player.getWorld(), 0, 0, 0),
                    new Location(player.getWorld(), 0, 0, 0)));
            sender.sendMessage(ChatColor.GREEN + "Created a room with id " + args[1].toUpperCase());
            return true;
        }
        else if(args[0].equalsIgnoreCase("delete")) {
            if(args.length < 2) {
                sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " delete <id>");
                return true;
            }

            if(manager.getRoom(args[1]) == null) {
                sender.sendMessage(ChatColor.RED + "No room with that id exists");
                return true;
            }

            manager.unregisterRoom(args[1]);
            sender.sendMessage(ChatColor.GREEN + "Deleted the room with id " + args[1].toUpperCase());
            return true;
        }
        else if(args[0].equalsIgnoreCase("setbutton")) {
            if(args.length < 2) {
                sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " setbutton <id>");
                return true;
            }

            ShulkerRoom room = manager.getRoom(args[1]);
            if(room == null) {
                sender.sendMessage(ChatColor.RED + "No room with that id exists");
                return true;
            }

            Block block = player.getTargetBlockExact(5, FluidCollisionMode.NEVER);
            if(block == null) {
                sender.sendMessage(ChatColor.RED + "You must be looking at a block to use this command");
                return true;
            }

            room.setButtonLoc(block.getLocation());
            sender.sendMessage(ChatColor.GREEN + "Set button location of " + args[1].toUpperCase() + " to the block you're looking at");
            return true;
        }
        else if(args[0].equalsIgnoreCase("setlocation")) {
            if(args.length < 2) {
                sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " setlocation <id>");
                return true;
            }

            ShulkerRoom room = manager.getRoom(args[1]);
            if(room == null) {
                sender.sendMessage(ChatColor.RED + "No room with that id exists");
                return true;
            }

            room.setTpLoc(player.getLocation());
            sender.sendMessage(ChatColor.GREEN + "Set location of " + args[1].toUpperCase() + " to your current position");
            return true;
        }
        else if (args[0].equalsIgnoreCase("tp")) {
            if(args.length < 2) {
                sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " tp <id>");
                return true;
            }

            ShulkerRoom room = manager.getRoom(args[1]);
            if(room == null) {
                sender.sendMessage(ChatColor.RED + "No room with that id exists");
                return true;
            }

            player.teleport(room.getTploc());
            sender.sendMessage(ChatColor.GREEN + "Teleported you to " + args[1].toUpperCase());
            return true;
        } else {
            sender.sendMessage(ChatColor.GREEN + "Shulker room help:\n" +
                    " /shulker setfreeloc\n" +
                    " /shulker setblock\n" +
                    " /shulker create <id>\n" +
                    " /shulker delete <id>\n" +
                    " /shulker setbutton <id>\n" +
                    " /shulker setlocation <id>\n" +
                    " /shulker tp <id>");
            return true;
        }

    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if(args.length == 1) {
            return Stream.of("setfreeloc", "setblock", "create", "delete", "setbutton", "setlocation", "tp")
                    .filter(c -> c.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }
        else if(args.length == 2 && Arrays.asList("delete", "setbutton", "setlocation", "tp").contains(args[0].toLowerCase())) {
            return manager.getStorage().getShulkerRooms().keySet().stream().toList().stream()
                    .filter(c -> c.toLowerCase().startsWith(args[1].toLowerCase()))
                    .collect(Collectors.toList());
        }
        return null;
    }
}
